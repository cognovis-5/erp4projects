FROM sussdorff/project-open

RUN  apt-get update -y && apt-get -y dist-upgrade && apt-get install -y tzdata && apt-get -y autoremove
ENV TZ Europe/Berlin
WORKDIR /var/www/openacs/packages

# Packages to overwrite
ENV PKGS_LIST "cognovis-core cognovis-rest intranet-invoices intranet-openoffice intranet-material intranet-chilkat intranet-fs intranet-collmex"
ENV PKGS_OLD_LIST "intranet-freelance intranet-freelance-invoices"
ENV DEPRECATE_LIST "views intranet-jquery intranet-mail"

RUN for pkg in ${PKGS_LIST} ; do echo $pkg \
    && rm -rf $pkg && wget https://gitlab.com/cognovis-5/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done && \
    for pkg in ${PKGS_OLD_LIST} ; do echo $pkg \
    && rm -rf $pkg && wget -q https://gitlab.com/cognovis/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done && \
    for pkg in ${DEPRECATE_LIST} ; do echo $pkg \
    && rm -rf $pkg && wget -q https://gitlab.com/cognovis/$pkg/-/archive/master/$pkg.tar.gz \
    && tar xfz $pkg.tar.gz && mv ${pkg}-master-* $pkg && rm $pkg.tar.gz ; done
